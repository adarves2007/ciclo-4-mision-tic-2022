const express = require('express');
const router = express.Router();
const auth = require("../middleware/auth");

//CONFIGURACION DE LA RUTA CON EL METODO <<SUGERIDO>> POR EL PROFESOR
const controladorProductos = require('../controllers/controller_productos');
router.get("/listar",auth,controladorProductos);
router.get("/listarcombo",auth,controladorProductos);
router.get("/cargar/:id",auth,controladorProductos);
router.post("/agregar",auth,controladorProductos);
router.post("/editar/:id",auth,controladorProductos);
router.delete("/borrar/:id",auth,controladorProductos);

//CONFIGURACION DE LA RUTA CON EL METODO <<SUGERIDO>> POR EL PROFESOR
const controladorProductosSinAuth = require('../controllers/controller_productos');
router.get("/listarsinauth",controladorProductosSinAuth);
router.get("/cargarsinauth/:id",controladorProductosSinAuth);
/*
router.post("/agregarsinauth",controladorProductosSinAuth);
router.post("/editarsinauth/:id",controladorProductosSinAuth);
router.delete("/borrarsinauth/:id",controladorProductosSinAuth);
*/

//CONFIGURACION DE LA RUTA CON EL METODO SUGERIDO EN LA GUIA
const controladorProductosGuia = require('../controllers/controller_productos_guia');
router.get("/listarguia/:id?",auth,controladorProductosGuia.productosListar);
router.post("/agregarguia/:id?",auth,controladorProductosGuia.productosAgregar);
router.post("/editarguia/:id?",auth,controladorProductosGuia.productosEditar);
router.delete("/borrarguia/:id?",auth,controladorProductosGuia.productosBorrar);

//CONFIGURACION DE LA RUTA CON EL METODO LEONARDO
const controladorProductosLeonardo = require('../controllers/controller_productos_leonardo');
router.get("/listarleonardo/:id?",auth,controladorProductosLeonardo.productosListar);
router.get("/cargarleonardo/:id?",auth,controladorProductosLeonardo.productosCargar);
router.post("/agregarleonardo/:id?",auth,controladorProductosLeonardo.productosAgregar);
router.post("/editarleonardo/:id?",auth,controladorProductosLeonardo.productosEditar);
router.delete("/borrarleonardo/:id?",auth,controladorProductosLeonardo.productosBorrar);

//CONFIGURACIÓN DE LAS RUTAS PARA REALIZAR CONSULTAS
const controladorProductosCategorias = require('../controllers/controller_productoscategorias');
router.get("/productoscategorias",auth,controladorProductosCategorias);
router.get("/productoscategorias/:id?",auth,controladorProductosCategorias);

module.exports = router
