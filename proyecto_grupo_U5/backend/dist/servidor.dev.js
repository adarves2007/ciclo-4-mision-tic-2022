"use strict";

var express = require('express');

var app = express();

var cors = require("cors"); //Habilitar cors


app.use(cors()); //Importo la conexion con mongoDB

var miconexion = require('./conexion'); //Importo el body parser


var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

var rutas = require('./routers/routers');

app.use('/api', rutas); //Peticion get por defecto

app.get('/', function (req, res) {
  res.end("Servidor Backend OK!");
}); //Servidor 

app.listen(5000, function () {
  console.log("Servidor OK en puerto 5000 - http://localhost:5000");
});