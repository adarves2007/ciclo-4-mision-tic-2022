import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import ProductosBorrar from './productosborrar';
import Menu from '../plantilla/menu';
import Logo from '../plantilla/logo';
import ProductosCombo from './productoscombo';


//import ProductosDetalle from '../componentes/productosdetalle';
//const navegar = useNavigate();
//navegar(0)


function ProductosListar()
{
    const token = localStorage.getItem("token");
    let bearer;
    if (token === "") {
        bearer = "";
    } else {
        bearer = `${token}`;
    }
    const config = {
        headers: {
            'Content-Type': 'application/json', 
            'x-auth-token': bearer}
    }

    const[dataProductos, setdataProductos] = useState([]);
    //Peticion GET para listar productos utilizando axios
    useEffect(()=>{axios.get('api/productos/listar',config).then(res => {
        console.log(res.data)
        setdataProductos(res.data)
    }).catch(err=>{console.log(err)})
    },[]);
    
    //Estructura para dibujar fondos de diversos colores en la tabla
    const tabla = document.getElementsByTagName("tr");
    //tabla[i].style.backgroundColor = "#888888";
    let i, j=0, fila=[], color=[], inicio=0;
    color[0]="table-info";color[1]="";color[2]="table-success";color[3]="";color[4]="table-danger";color[5]="";color[6]="table-warning";color[7]="";color[8]="table-active";color[9]="";
    for(i=0;i<tabla.length;i++)
    {
        fila[i]=color[j];
        j++;
        if(j==10){j=0;}
    }

    return(
        <div className="">
        <Menu/>
        <Logo/>
        <section className="pcoded-main-container">
            
            <div className="pcoded-content">
                <div className="page-header">
                    <div className="page-block">
                        <div className="row align-items-center">
                            <div className="col-md-12">
                                <div className="page-header-title">
                                    <h5 className="m-b-10">Mi tienda virtual</h5>
                                </div>
                                <ul className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to={"/inicio"}><i className="feather icon-home"></i></Link></li>
                                    <li className="breadcrumb-item"><Link to={"#"}>Inventario</Link></li>
                                    <li className="breadcrumb-item"><Link to={"/productos/listar"}>Productos</Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-header">
                            <h5>Listado de Productos</h5>
                            <span className="d-block m-t-5"></span>
                        </div>
                        <div className="card-body table-border-style">
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                        <tr key={0}>
                                            <td colSpan={6} align="right"><Link to={`/productosagregar`}><li className='btn btn-success'>Agregar Producto</li></Link></td>
                                        </tr>
                                        <tr key={1}>
                                            <td align="center">Id</td>
                                            <td>Nombre</td>
                                            <td align="center">Precio</td>
                                            <td align="center">Activo</td>
                                            <td align="center"></td>
                                            <td align="center"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {
                                    dataProductos.map((miproducto, indice) => (
                                        <tr key={miproducto.id} className={`${fila[indice]}`}>
                                            <td align="center">{miproducto.id}</td>
                                            <td>{miproducto.nombre}</td>
                                            <td align="right">{miproducto.precio}</td>
                                            <td align="center">{miproducto.activo ? 'Activo' : 'Inactivo'}</td>
                                            <td align="center"><Link to={`/productoseditar/${miproducto.id}`}><button type="button" className="btn  btn-icon btn-primary"><i className="feather icon-edit"></i></button></Link></td>
                                            <td align="center"><button type="button" className="btn btn-icon btn-danger" onClick={()=>{ProductosBorrar(miproducto.id)}}><i className="feather icon-trash"></i></button></td>                                           
                                        </tr>
                                        ))
                                    }
                                    <ProductosCombo/>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </section>
    
    </div>
    )

}

/*
function productosBorrar(id_borrar)
{

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: false
        }).then((result) => {
        if (result.isConfirmed) {
        //axios.delete('/api/productos/borrar',{id:id_borrar}).then(res=> {
        axios.delete(`/api/productos/borrar/${id_borrar}`).then(res=> {
            console.log(res.data)
            ProductosListar();
        }).catch(err=>{console.log(err)})
        swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
        )
        } else if (
        result.dismiss === Swal.DismissReason.cancel
        ) {
        swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
            )
        }
    })
}
*/

//ENVIO DE PARAMETROS AL COMPONENTE
/*

const listaproductos = dataProductos.map(miproducto => {return(
    <div>
        <ProductosDetalle miproducto={miproducto}/>
    </div>
)
})
return(
        <div>
            <h1>Lista de Productos</h1>
            {listaproductos}
        </div>
    )
*/

//<td><img src={`${process.env.PUBLIC_URL}/imagenes/${miproducto.img}`} alt={miproducto.nombre} width="30px" className="img-fluid"/></td>

export default ProductosListar;

