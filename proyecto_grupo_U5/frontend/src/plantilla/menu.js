import {Link} from 'react-router-dom';

function Menu()
{

return(
    <div className="">
        <nav className="pcoded-navbar">
            <div className="navbar-wrapper">
                <div className="navbar-content scroll-div">
                    <ul className="nav pcoded-inner-navbar ">
                        <li className="nav-item pcoded-menu-caption">
                            <label>Tienda Virtual</label>
                        </li>
                        <li className="nav-item">
                        <Link to={"/inicio"} className="nav-link"><span className="pcoded-micon"><i className="feather icon-home" /></span><span className="pcoded-mtext">Inicio</span></Link>
                        </li>
                        <li className="nav-item pcoded-menu-caption">
                            <label>Inventario</label>
                        </li>
                        <li className="nav-item">
                        <Link to={"/productoslistar"} className="nav-link"><span className="pcoded-micon"><i className="feather icon-file-text" /></span><span className="pcoded-mtext">Productos</span></Link>
                        </li>
                        <li className="nav-item">
                        <Link to={"/categoriaslistar"} className="nav-link"><span className="pcoded-micon"><i className="feather icon-align-justify" /></span><span className="pcoded-mtext">Categorias</span></Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    )

}

/*
    <div className="">
        <nav className="pcoded-navbar">
        <div className="navbar-wrapper">
            <div className="navbar-content scroll-div">
                <ul className="nav pcoded-inner-navbar ">
                    <li className="nav-item pcoded-menu-caption">
                        <label>Administración</label>
                    </li>
                    <li className="nav-item">
                        <a href="index.html" className="nav-link"><span className="pcoded-micon"><i className="feather icon-home"></i></span><span className="pcoded-mtext"><Link to={"/inicio"}>Inicio</Link></span></a>
                    </li>
                    <li className="nav-item pcoded-hasmenu">
                        <a href="#!" className="nav-link "><span className="pcoded-micon"><i className="feather icon-layout"></i></span><span className="pcoded-mtext">Inventario</span></a>
                        <ul className="pcoded-submenu">
                            <li><a href="layout-vertical.html" target="_blank">Productos</a></li>
                            <li><a href="layout-horizontal.html" target="_blank">Categorias</a></li>
                        </ul>
                    </li>
                    <li className="nav-item pcoded-menu-caption">
                        <label>Comercial</label>
                    </li>
                    <li className="nav-item pcoded-hasmenu">
                        <a href="#!" className="nav-link "><span className="pcoded-micon"><i className="feather icon-box"></i></span><span className="pcoded-mtext">Ventas</span></a>
                        <ul className="pcoded-submenu">
                            <li><a href="bc_alert.html">Clientes</a></li>
                            <li><a href="bc_button.html">Facturación</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        </nav>
    </div>
*/

export default Menu
