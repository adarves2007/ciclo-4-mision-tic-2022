import React, { useEffect } from 'react';

/*
function PaginaPrincipal()
{

    return(
        <div className="container mt-5">
            <h4>Pagina Principal</h4>
            <div className="row">
                <div className="col-md-12">
                    Seleccione una opción del menú
                </div>
            </div>
        </div>
    )

}
export default PaginaPrincipal;
*/

function Ejemplo() 
{
    useEffect(function ()
    {
        console.log('render!')
    })

    return (<span>This is a useEffect example</span>)
}

export default Ejemplo;