const express = require('express');
const router = express.Router();

const rutaClientes = require('./router_clientes');
router.use("/clientes",rutaClientes);

const rutaPedidos = require('./router_pedidos');
router.use("/pedidos",rutaPedidos);

const rutaConsultas = require('./router_consultas');
router.use("/consultas",rutaConsultas);

const rutaUsuarios = require('./router_usuarios');
router.use("/usuarios",rutaUsuarios);

const rutaAuth = require('./router_auth');
router.use("/auth",rutaAuth);

module.exports = router;
