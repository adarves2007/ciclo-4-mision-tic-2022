const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const controladorPedidosClientes = require('../controllers/controller_pedidosclientes');
router.get("/pedidosclientes",auth,controladorPedidosClientes);
router.get("/pedidosclientes/:id",auth,controladorPedidosClientes);

module.exports = router;

